# SNSAnayser
**Excution Code**:
400: 최선 문장 리스트 요청 (cate_id, [max_res])

500: 전처리가 되지 않은 문장 전처리

600: 수집서버 시작
601: 수집서버 중지
602: 수집개체 추가(key_id)
603: 수집개체 제거(key_id)

**setting.json**:
```json
{
    "MainServerInfos": {
        "port": \\,
        "max_thread": \\,
        "Enable_white_list": \\,
        "white_list": [ \\ ],
        "Enable_black_list": \\,
        "black_list": [ \\ ]
    },
    "DBInfos": {
        "IP": \\,
        "port":\\,
        "id": \\,
        "pass": \\,
        "DB": \\
    },
    "DebugInfos": {
        "Elasticresearch": {
            "IP": \\,
            "port": \\
        },
        "Except": [ \\ ],
        "Level": \\ ,
        "_LEVEL_COMMENT": "ERROR, PLAIN, VERBO, DEBUG, UNNEC"
    },
    "AnayserInfos": {
        "AllowWordTags": [ "nc", "f", "nq", "pv", "pa", "ma", "unk" ]
    }
}
```