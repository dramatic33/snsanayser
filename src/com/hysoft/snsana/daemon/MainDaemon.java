package com.hysoft.snsana.daemon;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;

import com.hysoft.snsana.db.DB;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.StringSET;
import com.hysoft.snsana.etc.Setter;
import com.hysoft.snsana.server.MainServer;
import com.hysoft.snsana.server.MainServerFactory;

public class MainDaemon implements Daemon{
	private MainServer server = null;
	private final String DBG_TAG = StringSET.DAEMON.TAG;
	@Override
	public void destroy() {
		Logger.i(DBG_TAG, StringSET.DAEMON.DESTROY);
	}

	@Override
	public void init(DaemonContext context) {
		try{
			Setter setter = new Setter("setting.json");
			Logger.i(DBG_TAG, StringSET.DAEMON.INITING);
			server = setter.getServer();
			
			Logger.i(DBG_TAG, StringSET.DAEMON.INITED);
		} catch (Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void start() throws Exception {
		Logger.i(DBG_TAG, StringSET.DAEMON.STARTING);
		server.start();
		Logger.i(DBG_TAG, StringSET.DAEMON.STARTED);
	}

	@Override
	public void stop() throws Exception {
		Logger.i(DBG_TAG, StringSET.DAEMON.STOPPING);
		server.stop();
		Logger.i(DBG_TAG, StringSET.DAEMON.STOPPED);
	}

}
