package com.hysoft.snsana.db;

import java.sql.Connection;

import com.hysoft.snsana.resmanager.AllocResource;

public class DBconnection implements AllocResource{
	public Connection con;
	public DBconnection(Connection connection) {
		this.con = connection;
	}

	@Override
	public Connection getResource() {
		return con;
	}

	@Override
	public void setResource(Object resource) {
		this.con = (Connection)resource;
	}

}
