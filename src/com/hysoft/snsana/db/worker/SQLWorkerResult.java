package com.hysoft.snsana.db.worker;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.resmanager.WorkerResult;

public class SQLWorkerResult implements WorkerResult{
	private ResultSet res = null;
	public SQLWorkerResult(ResultSet res){
		this.res = res;
	}
	public boolean next() throws SQLWorkerException{
		try{
			return res.next();
		} catch(SQLException e){
			throw new SQLWorkerException(e);
		}
	}
	public String getString(int column) throws SQLWorkerException{
		try{
			return res.getString(column);
		} catch(SQLException e){
			throw new SQLWorkerException(e);
		}
	}
	public int getInt(int column) throws SQLWorkerException{
		try{
			return res.getInt(column);			
		} catch(SQLException e){
			throw new SQLWorkerException(e);
		}
	}
	public String getString(String column_name) throws SQLWorkerException{
		try{
			return res.getString(column_name);
		} catch(SQLException e){
			throw new SQLWorkerException(e);
		}
	}
	public Timestamp getTimestamp(int column) throws SQLWorkerException{
		try{
			return res.getTimestamp(column);
		} catch(SQLException e){
			throw new SQLWorkerException(e);
		}
	}
	public int getRow() throws SQLWorkerException{
		try{
			return res.getRow();
		} catch(SQLException e){
			throw new SQLWorkerException(e);
		}
	}
}
