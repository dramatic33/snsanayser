package com.hysoft.snsana.db.worker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;

public class ReturnIDSQLWorker extends SQLWorker{
	private int id = -1;
	public ReturnIDSQLWorker(String Query) throws SQLWorkerException{
		super(Query);
	}
	@Override
	public SQLWorkerResult send(Connection con) throws SQLWorkerException{
		try{
			stm = con.prepareStatement(Query, Statement.RETURN_GENERATED_KEYS);
			((PreparedStatement)(stm)).executeUpdate();
			ResultSet ret = stm.getGeneratedKeys();
			if(ret.next()){
				id = ret.getInt(1);
			} else {
				throw new SQLWorkerException("Failed to return id "+Query);
			}
			return null;
		} catch (SQLException e){
			throw new SQLWorkerException(e, Query);
		}
		
	}
	public int getID(){
		return id;
	}
}
