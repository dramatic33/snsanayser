package com.hysoft.snsana.db.worker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.Callable;

import com.hysoft.snsana.db.DBconnection;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.resmanager.AllocResource;
import com.hysoft.snsana.resmanager.Worker;
import com.hysoft.snsana.resmanager.WorkerResult;

abstract public class SQLWorker extends Worker{
	protected String Query;
	protected Statement stm;
	protected ResultSet res;
	
	public SQLWorker(String Query){
		this.Query = Query;
	}
	public WorkerResult work() throws Exception{
		return (WorkerResult) send(((DBconnection) mCon).getResource());
	}
	abstract public SQLWorkerResult send(Connection con) throws SQLWorkerException;
	public void setQuery(String Query){
		this.Query = Query;
	}
	public void close() throws SQLWorkerException{
		try{
			if(res!=null) res.close();
			if(stm!=null) stm.close();
		} catch(SQLException e){
			throw new SQLWorkerException(e, Query);
		}

	}
}
