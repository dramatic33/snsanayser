package com.hysoft.snsana.db.worker;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.hysoft.snsana.db.DBconnection;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.resmanager.Executor;

public class DBExecutor extends Executor{
	private String ID;
	private String Pass;
	private String URL;
	
	public DBExecutor(String URL, String ID, String Pass) throws SQLWorkerException{
		this.URL = URL;
		this.ID = ID;
		this.Pass = Pass;
		try{
			mCon = new DBconnection(DriverManager.getConnection(URL, ID, Pass));
		} catch(SQLException e){
			throw new SQLWorkerException(e);
		}
	}
	@Override
	public Executor duplicate() throws SQLWorkerException {
		Executor clone = new DBExecutor(this.URL, this.ID, this.Pass);
		return clone;
	}
}
