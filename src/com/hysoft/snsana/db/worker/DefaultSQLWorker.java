package com.hysoft.snsana.db.worker;

import java.sql.Connection;
import java.sql.SQLException;

import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;

public class DefaultSQLWorker extends SQLWorker{

	public DefaultSQLWorker(String Query){
		super(Query);
	}
	@Override
	public SQLWorkerResult send(Connection con) throws SQLWorkerException{
		try{
			this.stm = con.createStatement();
			this.res = this.stm.executeQuery(Query);
			return new SQLWorkerResult(res);
		} catch (SQLException e){
			throw new SQLWorkerException(e, Query);
		}
	}

}
