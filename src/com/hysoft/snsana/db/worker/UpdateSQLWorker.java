package com.hysoft.snsana.db.worker;

import java.sql.Connection;
import java.sql.SQLException;

import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;

public class UpdateSQLWorker extends SQLWorker{
	public UpdateSQLWorker(String Query){
		super(Query);
	}
	@Override
	public SQLWorkerResult send(Connection con) throws SQLWorkerException{
		try{
			stm = con.createStatement();
			stm.execute(Query);
			return null;
		} catch (SQLException e){
			throw new SQLWorkerException(e, Query);
		}
	}
}
