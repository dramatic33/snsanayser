package com.hysoft.snsana.db;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;

public class DBFactory {
	private String mIP = "192.168.0.3";
	private int mPort = 3306;
	private String mID = null;
	private String mPass = null;
	private String DB = null;
	public DBFactory setIP(String IP){
		mIP = IP;
		return this;
	}
	public DBFactory setPort(int Port){
		mPort = Port;
		return this;
	}
	public DBFactory setID(String ID){
		mID = ID;
		return this;
	}
	public DBFactory setPass(String Pass){
		mPass = Pass;
		return this;
	}
	public DBFactory setDB(String DB){
		this.DB = DB;
		return this;
	}
	//----------------Build------------------------
	public DB build() throws Exception{
		return new DB("jdbc:mysql://"+mIP+":"+mPort+"/"+DB+"?useUnicode=true&characterEncoding=UTF-8", mID, mPass);
	}
	
}
