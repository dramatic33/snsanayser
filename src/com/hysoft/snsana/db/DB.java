package com.hysoft.snsana.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.hysoft.snsana.analysis.Analyser;
import com.hysoft.snsana.analysis.SNSComment;
import com.hysoft.snsana.analysis.score.Score;
import com.hysoft.snsana.analysis.Word;
import com.hysoft.snsana.db.worker.DBExecutor;
import com.hysoft.snsana.db.worker.DefaultSQLWorker;
import com.hysoft.snsana.db.worker.ReturnIDSQLWorker;
import com.hysoft.snsana.db.worker.SQLWorker;
import com.hysoft.snsana.db.worker.SQLWorkerResult;
import com.hysoft.snsana.db.worker.UpdateSQLWorker;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.StringSET;
import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.debug.Exceptions.NullIDSQLException;
import com.hysoft.snsana.debug.Exceptions.UnableScorerException;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.resmanager.Allocator;
import com.hysoft.snsana.resmanager.Executor;
import com.hysoft.snsana.server.getter.Getter;
import com.hysoft.snsana.sns.SNS;
import com.hysoft.snsana.sns.Twitter;
import com.hysoft.snsana.sns.search.SearchResult;
import com.hysoft.snsana.sns.search.SearchResultSET;

public class DB{
	public static int THREAD_NUM = 10; //TODO ThreadNumber Set
	private final static String TAG = StringSET.DB.TAG;
	public DBCategory DBCategory;
	public DBSNS DBSNS;
	public DBGetter DBGetter;
	private Allocator alloc;
	public DB(String URL, String ID, String Pass) throws Exception{
		DBExecutor sample = new DBExecutor(URL, ID, Pass);
		alloc = new Allocator(sample, THREAD_NUM);
		DBCategory = new DBCategory();
		DBSNS = new DBSNS();
		DBGetter = new DBGetter();
	}
	public SQLWorkerResult Execute(SQLWorker worker) throws SQLWorkerException{
		Executor exe = null;
		try{
			exe = (Executor) alloc.Request();
			return (SQLWorkerResult) exe.getRunner().submit(worker);
		} catch(SQLWorkerException e){
			throw e;
		} catch(SQLException e){
			throw new SQLWorkerException(e);
		} catch(Exception e){
			Logger.e(TAG, e);
			throw new SQLWorkerException(e.getCause());
		} finally{
			alloc.free(exe);
		}
	}
	public class DBCategory{
		private final static String SentenTable = "sentense";
		private final static String PreProcessTable = "pre_preprocess_sentence";
		private final static String WordTable = "word";
		private final static String KeyTable = "keyword";
		private final static String sentence_keyword_view = "sentence_in_keyword";
		public SNSComment readSentence(int id, Analyser analyser) throws SQLWorkerException, AnalysisException{
			String Query = "SELECT content, keyword, date FROM "+sentence_keyword_view+" WHERE id="+id;
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			SQLWorkerResult res = Execute(worker);
			try{
				if(res.next()){
					String sent = res.getString(1);
					String keyword = res.getString(2);
					Date date = res.getTimestamp(3);
					try{
						return SNSComment.Analysis(sent, date, id, keyword); //TODO
					} catch(AnalysisException e){
						markProcessed(id, -1);
						throw e;
					}
					
				} else {
					throw new SQLWorkerException(new NullIDSQLException(sentence_keyword_view, id));
				}
			} finally{
				worker.close();
			}
			
		}
		public int readPrePreProcessRowCount() throws SQLWorkerException{
			return readRowCount(PreProcessTable);
		}
		public int readRowCount(String TableName) throws SQLWorkerException{
			String Query = "SELECT COUNT(*) FROM "+TableName;
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(worker);
				if(res.next()){
					return res.getInt(1);
				} else {
					throw new SQLWorkerException("Count Exception in"+TableName);
				}
			} finally{
				worker.close();
			}

		}
		public String readKeyword(int id) throws SQLWorkerException{
			String Query = "SELECT keyword FROM "+KeyTable+" where id="+id;
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(worker);
				if(res.next()){
					return res.getString(1);
				} else {
					throw new SQLWorkerException(new NullIDSQLException(KeyTable, id));
				}
			} finally {
				worker.close();
			}
			
		}
		public List<SNSComment> readRecentPreProcessSentence(int key_id, int Count) throws AnalysisException, SQLWorkerException{
			List<SNSComment> list = new ArrayList<SNSComment>();
			String keyword = readKeyword(key_id);
			String Query = "call select_sentense("+key_id+", "+Count+")";
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(worker);
				Logger.d(TAG, "Recieve Result");
				while(res.next()){
					int id = res.getInt(1);
					String sent = res.getString(2);
					Date date = res.getTimestamp(3);
					try{
						SNSComment comment = SNSComment.Analysis(sent, date, id, keyword);
						Logger.o(comment);
						list.add(comment);
					} catch(AnalysisException e){
						markProcessed(id, -1);
						if(!e.Timeouted()) throw e;
					}
				}
				return list;
			} finally{
				worker.close();
			}
			
		}
		private void addSentToWord(int sent_id, int word_id) throws SQLWorkerException{
			String Query = "INSERT INTO sent_to_word (sent_id, word_id) VALUES ("+sent_id+", "+word_id+")";
			Logger.u(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new UpdateSQLWorker(Query);
			try{
				Execute(worker);
			} finally{
				worker.close();
			}
		}
		private void markProcessed(int sent_id, int mark) throws SQLWorkerException{
			String Query = "UPDATE "+SentenTable+" SET processed="+mark+" WHERE id="+sent_id;
			Logger.u(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new UpdateSQLWorker(Query);
			try{
				Execute(worker);
			} finally{
				worker.close();
			}
		}
		public void addSentenceWord(SNSComment comment) throws SQLWorkerException{
			if(comment.getWordList()!=null){
				for(Word word : comment.getWordList()){
					try{
						int Wid = InsertWord(word);
						addSentToWord(comment.getID(), Wid);
					} catch (SQLWorkerException e){
						switch(e.getErrorCode()){
							case 1267: break; //Illegal mix of collation;
							case 1406: break; //Too long data field;
							default: throw e;
						}
						Logger.e(TAG, "Unable to insert word %s (%s)", word.getWord(), word.getTag());
					}
				}
			}
			markProcessed(comment.getID(), 1);			
		}
		public List<SNSComment> readSentContainWord(int key_id, int word_id) throws SQLWorkerException{
			List<SNSComment> list = new ArrayList<SNSComment>();
			String Query = String.format("call sentense_with_word(%d, %d)", key_id, word_id);
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(worker);
				while(res.next()){
					list.add(new SNSComment(res.getString(2), res.getInt(1)));
				}
				return list;
			} finally {
				worker.close();
			}
		}
		//Return word id from DB. if not in there, insert word.
		private int InsertWord(Word word) throws SQLWorkerException{
			String Query = "SELECT id FROM "+WordTable+" WHERE word=\""+word.getWord()+"\" and tag=\""+word.getTag()+"\"";
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			DefaultSQLWorker FindWorker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(FindWorker);
				if(!res.next()){
					Logger.v(TAG, StringSET.DB.NEWWORD, word.getWord());
					Query = "INSERT INTO "+WordTable+" (word, tag) VALUES (\""+word.getWord()+"\", \""+word.getTag()+"\");";
					Logger.d(TAG, StringSET.DB.QUERY, Query);
					ReturnIDSQLWorker InsertWorker = new ReturnIDSQLWorker(Query);
					try{
						Execute(InsertWorker);
						return InsertWorker.getID();
					} catch(SQLWorkerException e){
						switch(e.getErrorCode()){
						 case 1062: return InsertWord(word);
						 default:throw e;
						}
					} finally{
						InsertWorker.close();
					}
				}
				
				return res.getInt(1);
			} finally {
				FindWorker.close();
			}
		}
		public void Categorize(int sent_id, int cate_id) throws SQLWorkerException{
			String Query = String.format("INSERT INTO cate_to_sent (sent_id, cate_id) VALUES (%d, %d) ON DUPLICATE KEY UPDATE cate_id=%d", sent_id, cate_id, cate_id);
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			UpdateSQLWorker worker = new UpdateSQLWorker(Query);
			try{
				Execute(worker);
			} finally{
				worker.close();
			}
		}
		public List<Score> getScoerer(int cate_id) throws SQLWorkerException, UnableScorerException{
			return getScoerer(cate_id, 0);
		}
		public List<Score> getScoerer(int cate_id, int try_count) throws SQLWorkerException, UnableScorerException{
			List<Score> scorer = new ArrayList<Score>();
			String Query = String.format("call word_count_in_cate(%d, %d, %d)", cate_id, try_count, 10);
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(worker);
				while(res.next()){
					scorer.add(new Score(res.getInt(1), res.getInt(2)));
				}
				if(scorer.size()==0) throw new UnableScorerException(cate_id);
				return scorer;
			} finally{
				worker.close();
			}
		}
		public int getKeyword(int cate_id) throws SQLWorkerException{
			String Query = String.format("SELECT get_null_cate(%d)", cate_id);
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(worker);
				if(res.next()){
					return res.getInt(1);
				}
				return -1;
			} finally{
				worker.close();
			}
		}
		public int getKeywordID(int cate_id) throws SQLWorkerException{
			String Query = String.format("SELECT key_id From cate where id="+cate_id);
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(worker);
				if(res.next()){
					return res.getInt(1);
				}
				return -1;
			} finally{
				worker.close();
			}
		}
	}
	public class DBSNS{
		final private String SNSTable = "SNSInfos";
		final private String KeyTable = "keyword";
		final private String SentenTable = "sentense";
		
		public SNS getSNS(int id) throws SQLWorkerException, IOException{
			String Query = "SELECT * FROM "+SNSTable+" WHERE id="+id;
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			return getSNSFromQuery(Query);
		}
		public SNS getSNSFromKey(int id) throws SQLWorkerException, IOException{
			String Query = "SELECT * FROM "+"keyword_detail"+" WHERE key_id="+id;
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			return getSNSFromQuery(Query);
		}
		public SNS getSNSFromQuery(String Query) throws SQLWorkerException, IOException{
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult rs = Execute(worker);
				SNS sns = null;
				if(rs.next()){
					SNS.Base base = SNS.Base.valueOf(rs.getString("base"));
					switch(base){
						case TWITTER: sns = new Twitter(rs.getString("auth"));
					}
				} else {
					throw new SQLWorkerException(new NullIDSQLException(Query));
				}
				return sns;
			} finally{
				worker.close();
			}
		}
		public String getKeyword(int id) throws SQLWorkerException{
			String Query = "SELECT keyword FROM "+KeyTable+" WHERE id="+id;
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult rs = Execute(worker);
				if(rs.next()){
					return rs.getString(1);
				} else {
					return null;
				}
			} finally {
				worker.close();
			}
			
		}
		public void addSearchSQLWorkerResult(SearchResultSET set, int key_id, String keyword) throws AnalysisException, SQLWorkerException{
			for(SearchResult res : set){
				addSearchResult(res, key_id, keyword);
			}
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		public SNSComment addSearchResult(SearchResult res, int key_id, String keyword) throws AnalysisException, SQLWorkerException{
			String Query = String.format("INSERT INTO %s (content, date, key_id, selected) VALUES (\"%s\", %s, %s, 1)", SentenTable, res.getComment().replace("\"", "\"\""), format.format(res.getDate()), Integer.toString(key_id));
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			ReturnIDSQLWorker worker = new ReturnIDSQLWorker(Query);
			try{
				Execute(worker);
				int id = worker.getID();
				SNSComment comment = null;
				try{
					comment = SNSComment.Analysis(res.getComment(), res.getDate(), id, keyword);
				} catch (AnalysisException e){
					DBCategory.markProcessed(id, -1);
					throw e;
				}
				return comment;
			} finally {
				worker.close();
			}
			
		}
	}
	public class DBGetter{
		final private String PolicyTable = "keyword_search_policy";
		final private Timestamp NullDate = new Timestamp(-32400000);
		public List<Getter> getGetterList() throws SQLWorkerException{
			List<Getter> list = new ArrayList<Getter>();
			String Query = "SELECT id, start_time, end_time, max_count, keyword, null_cate FROM "+PolicyTable;
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(worker);
				while(res.next()){
					int id = res.getInt(1);
					int nullCate = res.getInt(6);
					String keyword = res.getString(5);
					Timestamp FromDate = res.getTimestamp(2);
					Timestamp ToDate = res.getTimestamp(3);
					Getter getter = null;
					if(!FromDate.equals(NullDate) && !ToDate.equals(NullDate)){
						getter = new Getter(id, keyword, nullCate);
						getter.SetDate(FromDate, ToDate);;
					} else {
						getter = new Getter(id, keyword, nullCate);
					}
					getter.setCount(res.getInt(4));
					list.add(getter);
				}
				return list;
			} finally{
				worker.close();
			}

			
		}
		public Getter getGetter(int key_id) throws SQLWorkerException{
			String Query = "SELECT id, start_time, end_time, max_count, keyword, null_cate FROM "+PolicyTable+" where id="+key_id;
			Logger.d(TAG, StringSET.DB.QUERY, Query);
			SQLWorker worker = new DefaultSQLWorker(Query);
			try{
				SQLWorkerResult res = Execute(worker);
				Getter getter = null;
				if(res.next()){
					int id = res.getInt(1);
					String keyword = res.getString(5);
					int nullCate = res.getInt(6);
					Timestamp FromDate = res.getTimestamp(2);
					Timestamp ToDate = res.getTimestamp(3);
					if(!FromDate.equals(NullDate) && !ToDate.equals(NullDate)){
						getter = new Getter(id, keyword, nullCate);
						getter.SetDate(FromDate, ToDate);;
					} else {
						getter = new Getter(id, keyword, nullCate);
					}
					getter.setCount(res.getInt(4));
				}
				return getter;
			} finally {
				worker.close();
			}
		}
	}
}
