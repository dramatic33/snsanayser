package com.hysoft.snsana.analysis;

import java.util.List;

import com.hysoft.snsana.resmanager.WorkerResult;

public class AnalyseWorkerResult implements WorkerResult{
	private List<Word> wordlist;
	public AnalyseWorkerResult(List<Word> wordlist){
		this.wordlist = wordlist;
	}
	public List<Word> getWordList(){
		return this.wordlist;
	}
}
