package com.hysoft.snsana.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;

import kr.ac.kaist.swrc.jhannanum.comm.Eojeol;
import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;

import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.resmanager.AllocResource;
import com.hysoft.snsana.resmanager.Worker;
import com.hysoft.snsana.resmanager.WorkerResult;

public class AnalyseWorker extends Worker{
	private String Sentence;
	//private String keyword;
	private static final String TAG = "EXT";
	
	public AnalyseWorker(String Sentence, String keyword){
		this.Sentence = Sentence;
		//this.keyword = keyword;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public WorkerResult work() throws Exception {
		final Workflow workflow = (Workflow) mCon.getResource();
		ExecutorService executor = Executors.newSingleThreadExecutor();
		try{
			Logger.d(TAG, "Try to extract word Extract :\t"+Sentence);
			Callable<List> callable = new Callable<List>() {
			    @Override
			    public List call() throws Exception {
			    	try{
			    		workflow.analyze(preProcess(Sentence));
				        return workflow.getResultOfDocument(new Sentence(0, 0, false));
			    	}catch(Exception e){
			    		throw e;
			    	}
			    	
			    }
			};
			Future<List> future = executor.submit(callable);
			List<Word> res = new ArrayList<Word>();
			List<Sentence> ret = future.get(10, TimeUnit.SECONDS);
			executor.shutdown();
			
			for(Sentence sent:ret){
				Eojeol[] EojeolList = sent.getEojeols();
				for(Eojeol eo:EojeolList){
					 for(int i=0;i<eo.length;i++){
						 if(matchTag(eo.getTag(i))){
							 //if(!eo.getMorpheme(i).equals(keyword)){
								 res.add(new Word(eo.getMorpheme(i), eo.getTag(i))); 
							 //}
						 }
					 }
				 }
			}
			Logger.d(TAG, "Word Extract Complete:"+Sentence);
			return (WorkerResult) new AnalyseWorkerResult(res);
		} catch (TimeoutException e){
			executor.shutdown();
			Logger.e(TAG, new Exception("Timeout in extract word sent:"+Sentence));
			WorkflowRes.destroyWorkFlow((WorkflowRes) mCon);
			mCon = WorkflowRes.createWorkFlow();
			throw new AnalysisException(e, Sentence);
		} catch (InterruptedException | ExecutionException e) {
			executor.shutdown();
			throw new AnalysisException(e, Sentence);
		} catch (Exception e){
			executor.shutdown();
			throw new AnalysisException(e);
		}
	}
	private static boolean matchTag(String Tag){
		for(String Allows:Analyser.AllowTags){
			if(Tag.startsWith(Allows)) return true;
		}
		return false;
	}
	private static final String URL = "(http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?";
	private static String preProcess(String Document){
		return removeURL(Document).replace("\"", "").replace("\'", "");
	}
	private static String removeURL(String Document){
		return Document.replaceAll(URL, "");
	}

}
