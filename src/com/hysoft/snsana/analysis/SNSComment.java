package com.hysoft.snsana.analysis;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.server.MainServer;

public class SNSComment{
	private String sent;
	private int id;
	private int score;
	private Date postDate;
	
	private List<Word> wordList = new ArrayList<Word>();
	public SNSComment(String sent, int id){
		this.sent = sent;
		this.id = id;
	}
	public static SNSComment Analysis(String sent, Date date, int id, String keyword) throws AnalysisException{
		sent = sent.replaceAll("\n", " ");
		SNSComment comment = new SNSComment(sent, id);
		comment.postDate = date;
		comment.wordList = MainServer.analyser.AnalyseSentence(sent, keyword);
		return comment;
	}
	public Date getPostDate(){
		return this.postDate;
	}
	public List<Word> getWordList(){
		return this.wordList;
	}
	public String getComment(){
		return this.sent;
	}
	public int getID(){
		return this.id;
	}
	public void setScore(int Score){
		this.score = Score;
	}
	public int getScore(){
		return this.score;
	}
	public boolean contain(String keyword){
		boolean contain = false;
		for(Word word:this.getWordList()){
			if(word.getWord().equals(keyword)){
				contain = true;
				break;
			}
		}
		return contain;
	}
}
