package com.hysoft.snsana.analysis;

public class Word {
	private String word;
	private String Tag;
	public Word(String word, String Tag){
		this.word = word;
		this.Tag = Tag;
	}
	public String getWord(){
		return word;
	}
	public String getTag(){
		return Tag;
	}
	@Override
	public String toString(){
		return "("+Tag+")"+this.word;
	}
}
