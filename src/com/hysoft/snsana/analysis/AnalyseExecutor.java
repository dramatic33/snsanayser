package com.hysoft.snsana.analysis;

import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.resmanager.Allocatable;
import com.hysoft.snsana.resmanager.Executor;

public class AnalyseExecutor extends Executor{
	public AnalyseExecutor() throws AnalysisException{
		super.mCon = WorkflowRes.createWorkFlow();
	}
	@Override
	public Allocatable duplicate() throws Exception {
		return new AnalyseExecutor();
	}
	

}
