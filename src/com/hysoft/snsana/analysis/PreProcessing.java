package com.hysoft.snsana.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.server.MainServer;
import com.hysoft.snsana.server.getter.Getter;

public class PreProcessing implements Runnable{
	private final static int THREAD_NUM = 40; //TODO: ThreadNumber Set
	private int interval;
	private int key_id;
	private String keyword;
	private int nullCate;
	public PreProcessing(int key_id, int interval) throws SQLWorkerException{
		this.interval = interval;
		this.key_id = key_id;
		Getter getter = MainServer.mDB.DBGetter.getGetter(key_id);
		this.key_id = getter.getID();
		this.nullCate = getter.getNullCate();
	}
	private class PreProcessingExecutor implements Callable<Boolean>{
		private SNSComment comment;
		public PreProcessingExecutor(SNSComment Comment){
			this.comment = Comment;
			Thread.currentThread().setName("PreProcessingExecutor");
		}
		@Override
		public Boolean call() throws Exception {
			try{
				MainServer.mDB.DBCategory.addSentenceWord(comment);
				if(!comment.contain(keyword)){
					MainServer.mDB.DBCategory.Categorize(comment.getID(), nullCate);
				}
				return true;
			} catch (SQLWorkerException e){
				Logger.e("ANA", e);
				return true;
			} catch(Exception e){
				throw e;
			}
		}
	}
	private class ListGetter implements Callable<List<SNSComment>>{
		@Override
		public List<SNSComment> call() throws Exception{
			try{
				return MainServer.mDB.DBCategory.readRecentPreProcessSentence(key_id, interval);
			} catch (SQLWorkerException e){
				Logger.e("ANA", e);
				return null;
			} catch(Exception e){
				throw e;
			}
		}
	}
	@Override
	public void run() {
		try{
			int rowcount = MainServer.mDB.DBCategory.readPrePreProcessRowCount();
			int count = 0;
			
			ExecutorService executor = Executors.newFixedThreadPool(THREAD_NUM);
			ExecutorService Listexecutor = Executors.newSingleThreadExecutor();
			Logger.v("ANA", "Process preprocessed sentence (%s of %s)",count, rowcount);
			List<SNSComment> list = new ListGetter().call();
			while(true){
				if(list.size()==0) break;
				count += interval;
				List<PreProcessingExecutor> jobs = new ArrayList<PreProcessingExecutor>();
				for(SNSComment comment:list){
					jobs.add(new PreProcessingExecutor(comment));
				}
				try{
					Logger.v("ANA", "Process preprocessed sentence (%s of %s)",count, rowcount);
					Future<List<SNSComment>> res = Listexecutor.submit(new ListGetter());
					executor.invokeAll(jobs);
					Logger.d("ANA", "Query Finished");
					System.gc();
					list = res.get();
					Logger.d("ANA", "List get Finished");
				} catch (InterruptedException e){
					throw new SQLWorkerException(e);
				}
			}
			Logger.v("ANA", "Finish preprocessed sentense");
		} catch (Exception e){
			Logger.e("ANA", e);
		}
		
	}
}
