package com.hysoft.snsana.analysis;

import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;
import kr.ac.kaist.swrc.jhannanum.plugin.MajorPlugin.MorphAnalyzer.ChartMorphAnalyzer.ChartMorphAnalyzer;
import kr.ac.kaist.swrc.jhannanum.plugin.MajorPlugin.PosTagger.HmmPosTagger.HMMTagger;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PlainTextProcessor.InformalSentenceFilter.InformalSentenceFilter;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PlainTextProcessor.SentenceSegmentor.SentenceSegmentor;

import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.resmanager.AllocResource;

public class WorkflowRes implements AllocResource{
	private SentenceSegmentor SS = null;
	private InformalSentenceFilter ISF = null;
	private ChartMorphAnalyzer CMA = null;
	private HMMTagger TAGGER = null;
	private Workflow res;
	@Override
	public Object getResource() {
		return res;
	}

	@Override
	public void setResource(Object resource) {
		this.res = (Workflow) resource;		
	}
	public static WorkflowRes createWorkFlow() throws AnalysisException{
		WorkflowRes res = new WorkflowRes();
		Workflow workflow = new Workflow();
		res.SS = new SentenceSegmentor();
		res.ISF = new InformalSentenceFilter();
		res.CMA = new ChartMorphAnalyzer();
		res.TAGGER = new HMMTagger();
		
		workflow.appendPlainTextProcessor(res.SS, null);
		workflow.appendPlainTextProcessor(res.ISF, null);
		workflow.setMorphAnalyzer(res.CMA, "conf/plugin/MajorPlugin/MorphAnalyzer/ChartMorphAnalyzer.json");
		workflow.setPosTagger(res.TAGGER, "conf/plugin/MajorPlugin/PosTagger/HmmPosTagger.json");
		res.res = workflow;
		try{
			workflow.activateWorkflow(true);
			return res;
		} catch (Exception e){
			throw new AnalysisException(e);
		}
	}
	public static void destroyWorkFlow(WorkflowRes res){
		res.res = null;
		res.SS = null;
		res.ISF = null;
		res.CMA = null;
		res.TAGGER = null;
		
		System.gc();
	}
}
