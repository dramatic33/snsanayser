package com.hysoft.snsana.analysis.score;

public class AcceptLowScoringPolict implements ScoringPolicy{
	int score_cutter = -1;
	private double ratio = 0.2;
	@Override
	public boolean checkScoreCut(int score) {
		return score!=1;
	}

	@Override
	public void updateScoreCut(int score) {
		score_cutter = (int) Math.floor (((double)score)*ratio);
	}

	@Override
	public int getScoreCut() {
		return 1;
	}
}
