package com.hysoft.snsana.analysis.score;

public interface ScoringPolicy {
	public boolean checkScoreCut(int score);
	public void updateScoreCut(int score);
	public int getScoreCut();
}
