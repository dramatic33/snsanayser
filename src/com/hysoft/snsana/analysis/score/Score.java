package com.hysoft.snsana.analysis.score;

public class Score{
	private int word_id;
	private int count;
	public Score(int word_id, int count){
		this.word_id = word_id;
		this.count = count;
	}
	public int getWordID(){
		return this.word_id;
	}
	public int getScore(){
		return count;
	}
}