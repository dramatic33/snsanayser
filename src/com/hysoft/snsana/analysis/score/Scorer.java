package com.hysoft.snsana.analysis.score;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.hysoft.snsana.analysis.SNSComment;
import com.hysoft.snsana.db.DB;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.debug.Exceptions.UnableScorerException;
import com.hysoft.snsana.server.MainServer;

public class Scorer {
	private static final String TAG = "ANA";
	private int cate_id;
	public Scorer (int cate_id){
		Logger.v(TAG, "Create Scorer "+cate_id);
		this.cate_id = cate_id;
	}
	public int getCateID(){
		return this.cate_id;
	}
	public List<SNSComment> getBestSentence(DB db, ScoringPolicy policy) throws SQLWorkerException{
		return getBestSentence(db, policy, -1);
	}
	private List<SNSComment> getBestSentence(DB db, ScoringPolicy policy, int max_res, final boolean reverse) throws SQLWorkerException{
		int key_id = MainServer.mDB.DBCategory.getKeywordID(cate_id);
		HashMap<SNSComment, Integer> list = new HashMap<SNSComment, Integer>();
		List<Score> score_book = db.DBCategory.getScoerer(cate_id);
		int try_count = 0;
		boolean EndGetScoerer = false;
		while(!EndGetScoerer){
			for(Score score:score_book){
				Logger.d(TAG, "(%s)\"%s\":score=%s",cate_id, score.getWordID(), score.getScore());
				List<SNSComment> contained_sent = db.DBCategory.readSentContainWord(key_id, score.getWordID());
				if(!policy.checkScoreCut(score.getScore())){
					Logger.d(TAG, "(%s)Scorer is in limit(%s < %s)", cate_id, score.getScore(), policy.getScoreCut());
					EndGetScoerer = true;
					break;
				}
				for(SNSComment sent_id:contained_sent){
					if(list.containsKey(sent_id)){
						list.put(sent_id, list.get(sent_id)+score.getScore());
						Logger.d(TAG, "\"%s\" increase score %s(%s)",sent_id.getID(), score.getScore(), list.get(sent_id));
					}
					else list.put(sent_id, score.getScore());
				}
				policy.updateScoreCut(score.getScore());
				Logger.d(TAG, "(%s)Update score cut to %s",cate_id, policy.getScoreCut());
			}
			if(!EndGetScoerer){
				try_count++;
				try{
					score_book = db.DBCategory.getScoerer(cate_id, try_count);
				} catch(UnableScorerException e){
					if(list.isEmpty()) throw e;
					EndGetScoerer = true;
					break;
				}
				
			}
		}
		List<Entry<SNSComment, Integer>> sorted = new LinkedList<Entry<SNSComment, Integer>>(list.entrySet());
		Collections.sort(sorted, new Comparator<Entry<SNSComment, Integer>>(){
			@Override
			public int compare(Entry<SNSComment, Integer> o1, Entry<SNSComment, Integer> o2) {
				if(reverse){
					return o1.getValue() - o2.getValue();
				} else {
					return o2.getValue() - o1.getValue();
				}
			}
		});
		//TODO CUT Result;
		if(max_res!=-1 && sorted.size() > max_res) sorted = sorted.subList(0, max_res);
		List<SNSComment> res = new ArrayList<SNSComment>();
		for(Entry<SNSComment, Integer> item:sorted){
			res.add(item.getKey());
			item.getKey().setScore(item.getValue());
		}
		return res;
	}
	public List<SNSComment> getBestSentence(DB db, ScoringPolicy policy, int max_res) throws SQLWorkerException{
		return getBestSentence(db, policy, max_res, false);
	}
	public List<SNSComment> getWorstSentence(DB db, ScoringPolicy policy, int max_res) throws SQLWorkerException{
		return getBestSentence(db, policy, max_res, true);
	}
}
