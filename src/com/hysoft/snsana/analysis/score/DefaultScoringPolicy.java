package com.hysoft.snsana.analysis.score;

public class DefaultScoringPolicy implements ScoringPolicy {
	int score_cutter = -1;
	private double ratio = 0.8;
	@Override
	public boolean checkScoreCut(int score) {
		return !(score_cutter>=0 && score <= score_cutter);
	}

	@Override
	public void updateScoreCut(int score) {
		score_cutter = (int) Math.floor (((double)score)*ratio);
	}

	@Override
	public int getScoreCut() {
		return score_cutter;
	}

}
