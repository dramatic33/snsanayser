package com.hysoft.snsana.analysis;

import java.util.List;

import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.resmanager.Allocator;

public class Analyser{
	public static int THREAD_NUM = 1;
	private Allocator alloc = null;
	static String[] AllowTags;
	
	public Analyser() throws Exception{
		AnalyseExecutor sample = new AnalyseExecutor();
		alloc = new Allocator(sample, THREAD_NUM);
	}
	public List<Word> AnalyseSentence(String Sentence, String keyword) throws AnalysisException{
		AnalyseWorker worker = new AnalyseWorker(Sentence, keyword);
		AnalyseExecutor exe = null;
		try {
			exe = (AnalyseExecutor) alloc.Request();
			return ((AnalyseWorkerResult) exe.getRunner().submit(worker)).getWordList();
		}  catch (AnalysisException e) {
			throw e;
		} catch(Exception e){
			Logger.e("ANA", e);
			throw new AnalysisException(e.getCause());
		} finally{
			alloc.free(exe);
		}
	}
	public static void setAllowTags(String[] list){
		AllowTags = list;
	}
	
	
}
