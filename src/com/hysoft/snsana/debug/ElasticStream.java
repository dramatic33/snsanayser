package com.hysoft.snsana.debug;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hysoft.snsana.analysis.SNSComment;
import com.hysoft.snsana.analysis.Word;
import com.hysoft.snsana.resmanager.Allocator;

public class ElasticStream{
	private SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd, HH:mm:ss:SSSZ", Locale.ENGLISH);
	private SimpleDateFormat index_format = new SimpleDateFormat("yyyy.MM.dd", Locale.ENGLISH);
	private TransportClient client;
	public ElasticStream(String IP, int port){
		Settings settings = ImmutableSettings.settingsBuilder().put("cluster.name", "SNSAna").build();
		client = new TransportClient(settings);
        client.addTransportAddress(new InetSocketTransportAddress(IP,port));
	}
	public JSONObject getSkeleton(Date cur) throws JSONException{
		JSONObject json = new JSONObject();
		json.put("@timestamp",format.format(cur));
		return json;
	}
	public void println(String tag, String level, String message){
		try{
			Date cur = new Date();
			JSONObject json = getSkeleton(cur);			
			json.put("message", message);
			json.put("tag", tag);
			json.put("level", level);
			client.prepareIndex("snsana_log-"+index_format.format(cur), "log").setSource(json.toString()).execute().actionGet();
		} catch(JSONException e){
			Logger.i("LOG", e);
		}
	}
	public void println(SNSComment comment){
		try{
			Date cur = comment.getPostDate();
			JSONObject json = getSkeleton(cur);
			json.put("message", comment.getComment());
			JSONArray wordlist = new JSONArray();
			for(Word word:comment.getWordList()){
				JSONObject wordJson = new JSONObject();
				wordJson.put("word", word.getWord());
				wordJson.put("word_tag", word.getTag());
				wordlist.put(wordJson);
			}
			json.put("wordlist", wordlist);
			client.prepareIndex("snsana_get-"+index_format.format(cur), "sns_comment").setSource(json.toString()).execute().actionGet();
		} catch (JSONException e){
			Logger.i("LOG", e);
		}
	}
	public void println(Allocator alloc){
		try{
			Date cur = new Date();
			JSONObject json = getSkeleton(cur);
			json.put("worker_queue", alloc.getWorkerQueueSize());
			json.put("avail_queue", alloc.getAllocatorSize());
			client.prepareIndex("snsana_system-"+index_format.format(cur), "db_allo_info").setSource(json.toString()).execute().actionGet();
		} catch (JSONException e){
			Logger.i("LOG", e);
		}
	}
}