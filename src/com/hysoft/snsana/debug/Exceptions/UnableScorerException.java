package com.hysoft.snsana.debug.Exceptions;

import java.sql.SQLException;

public class UnableScorerException extends SQLWorkerException{
	public UnableScorerException(int cate_id){
		super("There is no cate item "+cate_id);
	}
}
