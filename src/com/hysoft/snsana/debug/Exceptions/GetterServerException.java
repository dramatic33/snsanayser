package com.hysoft.snsana.debug.Exceptions;

public class GetterServerException extends SNSServerException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6656205557263487723L;

	public GetterServerException(String MSG) {
		super(MSG);
	}

}
