package com.hysoft.snsana.debug.Exceptions;

import java.util.concurrent.TimeoutException;

public class AnalysisException extends Exception{
	private Exception e;
	public AnalysisException(TimeoutException e) {
		this.e = e;
	}
	public AnalysisException(Exception e, String Sentense) {
		super(e.getMessage()+" "+Sentense);
		this.e = e;
	}
	public AnalysisException(Exception e) {
		super(e);
	}
	public AnalysisException(Throwable cause) {
		super(cause);
	}
	public boolean Timeouted(){
		return (e instanceof TimeoutException);
	}

}
