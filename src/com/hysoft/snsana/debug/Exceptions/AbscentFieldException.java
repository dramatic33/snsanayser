package com.hysoft.snsana.debug.Exceptions;

public class AbscentFieldException extends SNSServerException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8971809055251426405L;
	private String Field;
	public AbscentFieldException(String Field) {
		super("Need Field "+Field);
		this.Field = Field;
	}
	public String getField(){
		return Field;
	}

}
