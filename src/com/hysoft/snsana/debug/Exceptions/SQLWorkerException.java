package com.hysoft.snsana.debug.Exceptions;

import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

public class SQLWorkerException extends Exception{
	String Query = null;
	Exception e;
	public SQLWorkerException(SQLException e, String Query){
		super(e.getMessage()+"=>"+Query);
		this.e = e;
	}
	public SQLWorkerException(SQLException e){
		super(e);
		this.e = e;
	}
	public SQLWorkerException(NullIDSQLException e){
		super(e);
		this.e = e;
	}
	public SQLWorkerException(String Message){
		super(Message);
	}
	public SQLWorkerException(InterruptedException e) {
		super(e);
		this.e = e;
	}
	public SQLWorkerException(ExecutionException e) {
		super(e);
		this.e = e;
	}/*
	public SQLWorkerException(SQLWorkerException e) {
		super(e);
		this.e = e;
	}*/
	public SQLWorkerException(AnalysisException e2) {
		// TODO Auto-generated constructor stub
	}
	public SQLWorkerException(Throwable cause) {
		super(cause);
	}
	public Exception getException(){
		return e;
	}
	/*
	public SQLWorkerException(Exception e) {
		super(e);
		this.e = e;
	}*/
	public int getErrorCode(){
		if(e instanceof SQLException){
			return ((SQLException)e).getErrorCode();
		} else {
			return -1;
		}
	}
}
