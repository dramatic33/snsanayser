package com.hysoft.snsana.debug.Exceptions;

public class SNSServerException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -608763222547328856L;

	public SNSServerException(String MSG) {
		super(MSG);
	}

}
