package com.hysoft.snsana.debug.Exceptions;

import java.sql.SQLException;

public class NullIDSQLException extends SQLException{
	public NullIDSQLException(String table, int id){
		super("Call NullID "+id+" in "+table);
	}
	public NullIDSQLException(String Query){
		super("Call NullID "+Query);
	}
}
