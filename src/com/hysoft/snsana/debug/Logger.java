package com.hysoft.snsana.debug;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import com.hysoft.snsana.analysis.SNSComment;
import com.hysoft.snsana.resmanager.Allocator;

public class Logger {
	//public static final PrintStream stdoutput = System.out;
	//public static final PrintStream stderror = System.err;
	private static ElasticStream ElasticLogger;
	
	public static List<String> DEBUG_EXCEPT_LIST = new ArrayList<String>();
	private static final String[] LEVELS = {"ERROR", "PLAIN", "VERBO", "DEBUG", "UNNEC"};
	private static int level = 1;
	public static String ProcessMSG(Object... args){
		if(args.length == 1) return args[0].toString();
		Object[] argument = Arrays.copyOfRange(args, 1, args.length);
		
		return String.format(args[0].toString(), argument).replace("1970-01-01 ", "").replace(":00.0", "").replace("\n", "\t");
	}
	public static void i(String Field, Object... Message){
		if(level>0) print(Field, "PLAIN", ProcessMSG(Message));
	}
	public static void v(String Field, Object... Message){
		if(level>1)print(Field, "VERBO", ProcessMSG(Message));
	}
	public static void d(String Field, Object... Message){
		if(level>2) print(Field, "DEBUG", ProcessMSG(Message));
	}
	public static void u(String Field, Object... Message){
		if(level>3)print(Field, "UNNEC", ProcessMSG(Message));
	}
	
	public static void o(Object object){
		if(ElasticLogger!=null){
			if(object instanceof SNSComment) ElasticLogger.println((SNSComment)object);
			else if(object instanceof Allocator) ElasticLogger.println((Allocator)object);
			else {
				e("LOG", "Unknown instance of %s", object.getClass().getName());
			}
		}
	}
	private static void print(String Field, String level, String Message){
		if(!DEBUG_EXCEPT_LIST.contains(Field)){
			if(ElasticLogger!=null) ElasticLogger.println(Field, level, Message);
			else System.out.println("["+Field+"]\t{"+level+"}\t"+Message);
		} 
	}
	public static void e(String Field, Throwable e){
		String message = e.getMessage()!=null ? e.getMessage() : e.toString();
		printE(Field, ProcessMSG(message));
		for(StackTraceElement stack:e.getStackTrace()){
			printE(Field, ProcessMSG("(@ %s:%s)", stack.getClassName(), stack.getLineNumber()));
		}
	}
	public static void setElasticresearch(String IP, int port){
		ElasticLogger = new ElasticStream(IP, port);
	}
	public static void e(String Field, Object... Message){
		printE(Field, ProcessMSG(Message));
	}
	public static void printE(String Field, String Message){
		if(ElasticLogger!=null) ElasticLogger.println(Field, "ERROR", Message);
		else System.err.println("["+Field+"]\t{ERROR}\t"+Message);
	}
	public static void setLevel(String LevelStr){
		int i=0;
		for(i=0;i<LEVELS.length;i++){
			if(LevelStr.equals(LEVELS[i])) break;
		}
		level = i;
	}
}
