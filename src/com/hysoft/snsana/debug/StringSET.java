package com.hysoft.snsana.debug;

public class StringSET {
	public static class DAEMON{
		final public static String TAG = "DAE";
		
		final public static String INITING = "Initializing Daemon";
		final public static String INITED = "Initialized Daemon";
		final public static String STARTING = "Starting Daemon";
		final public static String STARTED = "Started Daemon";
		final public static String STOPPING = "Stopping Daemon";
		final public static String STOPPED = "Stopped Daemon";
		final public static String DESTROY = "Destoryed Daemon";
	}
	public static class SERVER{
		final public static String TAG = "SEV";
		
		final public static String INIT = "Initialize Server";
		final public static String START = "Start MainServer";
		final public static String STOP = "Stop MainServer";
		final public static String ACCEPT = "%s request connection";
		final public static String DISCONNECT = "%s was disconnected";
		
		final public static String REQNULL = "Request body is empty";
	}
	public static class DB{
		final public static String TAG = "DB";
		final public static String QUERY = "Send Query %s";
		final public static String NEWWORD = "There is no such word %s";
		final public static String NULL = "DB is not in there";
		
		final public static String NULLSNS = "There is no %s SNS";
	}
	public static class SNS{
		public static class TWITTER{
			final public static String TAG = "TWI";
			final public static String AUTH = "Authorization Complete with %s";
		}
	}
	public static class GET{
		final public static String TAG = "GET";
		final public static String START = "Start getter key id=%s";
		final public static String START_SERVER = "Start Getter Server";
		final public static String STOP_SERVER = "Stop Getter Server";
		final public static String NEW_FIXED = "New Getter key id=%s at %s to %s";
		final public static String NEW_ALWAYS = "New Getter key id=%s at always";
		final public static String REMOVE = "Remove Getter key id=%s";
		final public static String END = "End getter key id=%s";
		final public static String GET = "<%s>%s";
		final public static String INSERTERROR = "%s : %s";
		final public static String INFORM_NEXT = "Next key id %s's time is %s to %s";
	}
	public static class ANA{
		final public static String TAG = "ANA";
	}
}
