package com.hysoft.snsana.resmanager;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import com.hysoft.snsana.db.worker.SQLWorker;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.StringSET;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;

public class Allocator{
	private Allocatable[] ThreadPool=null;
	private Timer Refresher;
	
	private static final String TAG = StringSET.DB.TAG;
	private Queue<DBConAllocatorRunnable> workerQueue = new LinkedList<DBConAllocatorRunnable>();
	private Queue<Integer> availableQueue = new LinkedList<Integer>();
	
	public Allocator(Allocatable instance, int MaxThread) throws Exception{
		ThreadPool = new Allocatable[MaxThread];
		ThreadPool[0] = instance;
		availableQueue.add(0);
		for(int i=1;i<ThreadPool.length;i++){
			ThreadPool[i] = instance.duplicate();
			ThreadPool[i].setID(i);
			availableQueue.add(i);
		}
		
		Refresher = new Timer();
		Refresher.scheduleAtFixedRate(new ResidentExecutor(), 0, 1000*30);
	}
	public int find() {
		int id;
		if(availableQueue.isEmpty()) id= -1;
		else id = availableQueue.poll();
		if(id!=-1){
			ThreadPool[id].Start();
		}
		return id;
	}
	private ReentrantLock Alloclock = new ReentrantLock();
	private class DBConAllocatorRunnable implements Runnable{
		private Allocatable ret;
		private int waitID = -1;
		private int stage = 0;
		private SQLWorkerException exception = null;
		
		private ReentrantLock ReturnLock = new ReentrantLock();
		private Condition ReturnSignal = ReturnLock.newCondition();
		private Condition ReturnSignalOwn = ReturnLock.newCondition();
		private ReentrantLock WaitFindLock = new ReentrantLock();
		private Condition WaitFindSignal = WaitFindLock.newCondition();
		private String id = "";
		private Thread thread;
		public DBConAllocatorRunnable(String ID){
			this.id = ID;
		}
		public int waitFind() throws InterruptedException{
			Logger.u(TAG, "Try\tLock\tWaitFindLock\tWaiter\t%s\t%s",this.id,Thread.currentThread().getName());
			stage = 8; //Stage 8 Try to enter waitFindLock; 
			this.WaitFindLock.lock();
			try{
				stage = 9; //Stage 9 Successfully enter waitFindLock;
				Logger.u(TAG, "Success\tLock\tWaitFindLock\tWaiter\t%s\t%s",this.id,Thread.currentThread().getName());
				Alloclock.lock();
				workerQueue.add(this);
				Alloclock.unlock();
				Logger.u(TAG, "Try\tAwait\tWaitFindLock\tWaiter\t%s\t%s",this.id,Thread.currentThread().getName());
				stage = 10; //Stage 10 Await WaitFindSignal;
				this.WaitFindSignal.await();
				stage = 11; //Stage 11 Receive WaitFindSignal;
				Logger.u(TAG, "Get\tSignal\tWaitFindLock\tWaiter\t%s\t%s",this.id,Thread.currentThread().getName());
				int id = waitID;
				Logger.u(TAG, "Finished job. Dequeing and Get "+id);
				return id;
			} finally{
				Logger.u(TAG, "Try\tUnlock\tWaitFindLock\tWaiter\t%s\t%s",this.id,Thread.currentThread().getName());
				stage = 12; //Stage 12 Unlock WaitFindLock;
				this.WaitFindLock.unlock();
			}
		}
		public Allocatable Request() throws Exception{
			thread = new Thread(this);
			thread.start();
			//Stage 0 Start Allocator;
			stage = 0;
			Logger.u(TAG, "Try\tLock\tReturnLock\tGET\t%s\t%s",this.id,Thread.currentThread().getName());
			//Stage -1 Try Enter ReturnLock;
			stage = -1;
			this.ReturnLock.lock();
			//Stage 1 Success Enter ReturnLock;
			stage = 1;
			try{
				ReturnSignalOwn.signal();
				Logger.u(TAG, "Try\tAwait\tReturnLock\tGET\t%s\t%s",this.id,Thread.currentThread().getName());
				stage = 2; //Stage 2 Await Return Signal;
				this.ReturnSignal.await();
				stage = 18; //Stage 18 Recive ReturnLock Signal;
				Logger.u(TAG, "Get\tSignal\tReturnLock\tGET\t%s\t%s",this.id,Thread.currentThread().getName());
				if(this.exception!=null) throw this.exception;
				return ret;
			} catch (InterruptedException e){
				if(thread.isAlive()) thread.interrupt();
				throw new SQLWorkerException(e);
			} finally{
				this.exception = null;
				this.ret = null;
				Logger.u(TAG, "Try\tUnlock\tReturnLock\tGET\t%s\t%s",this.id,Thread.currentThread().getName());
				stage = 19; //Stage 18 Unlock ReturnLock;
				this.ReturnLock.unlock();
			}
			
		}
		@Override
		public void run() {
			try{
				stage=3; //Stage 3 Try enter Alloclock;
				Logger.u(TAG, "Try\tLock\tAllock\tRun\t%s\t%s",this.id,Thread.currentThread().getName());
				Alloclock.lock();
				stage=4; //Stage 4 Success enter Alloclock;
				int Retid = -1;
				try {
					stage = 5; //Stage 5 Try get Allocation;
					Logger.u(TAG, "Success\tLock\tAllock\tRun\t%s\t%s",this.id,Thread.currentThread().getName());
					Retid = find();
					Logger.u(TAG, "Try\tUnlock\tAllock\tRun\t%s\t%s",this.id,Thread.currentThread().getName());
				} finally {
					stage = 6; //Stage 6 Unlock AllocLock;
					Alloclock.unlock();
				}
				if(Retid==-1){
					stage = 7; //Stage 7 WaitFind; 
					Retid = waitFind();
				}
				//Stage -2
				stage = -2; //Successfully get ID;
				this.ret = ThreadPool[Retid];
				Logger.u(TAG, "Try\tLock\tReturnLock\tRun\t%s\t%s",this.id, Thread.currentThread().getName());
				stage = 13; //Stage 13 Try to enter ReturnLock Runner;
				this.ReturnLock.lock();
				try{
					stage = 14; //Stage 14 Enter ReturnLock Runner;
					if(!ReturnLock.hasWaiters(ReturnSignal)){
						stage = 15; //Stage 14 Await Unready receiver
						ReturnSignalOwn.await();
					}
					Logger.u(TAG, "Success\tLock\tReturnLock\tRun\t%s\t%s",this.id, Thread.currentThread().getName());
				}  finally {
					stage = 16; //Stage 16 Signal ReturnLock Run
					Logger.u(TAG, "Try\tSignal\tReturnLock\tRun\t%s\t%s",this.id,Thread.currentThread().getName());
					this.ReturnSignal.signal();
					stage = 17; //Stage 16 Unlock ReturnLock Run
					Logger.u(TAG, "Try\tUnlock\tReturnLock\tRun\t%s\t%s",this.id,Thread.currentThread().getName());
					this.ReturnLock.unlock();
				}
			} catch (InterruptedException e) {
				this.exception = new SQLWorkerException(e);
			}
		}
	}
	
	public Allocatable Request() throws Exception{
		return new DBConAllocatorRunnable(getSaltString()).Request();
	}
	public void free(Allocatable executor){
		Logger.u(TAG, "Try\tLock\tAllock\tFree\t \t%s", Thread.currentThread().getName());
		Alloclock.lock();
		executor.Stop();
		try{
			Logger.u(TAG, "Sucess\tLock\tAllock\tFree\t \t%s", Thread.currentThread().getName());
			int id = executor.getID();
			availableQueue.offer(id);
			if(!this.workerQueue.isEmpty()){
				DBConAllocatorRunnable head = this.workerQueue.poll();
				head.stage = 20; //Stage 20 Enter Free AllocLock;
				Logger.u(TAG, "Try\tLock\tWaitFindLock\tFree\t%s\t%s",head.id, Thread.currentThread().getName());
				head.stage = 21; //Stage 21 Try to Enter WaitFindLock Free
				head.WaitFindLock.lock();
				try{
					head.stage = 22; //Stage 22 Enter WaitFindLock Free
					Logger.u(TAG, "Success\tLock\tWaitFindLock\tFree\t%s\t%s",head.id, Thread.currentThread().getName());
					head.waitID = find();
					Logger.u(TAG, "Try\tSignal\tWaitFindLock\tFree\t%s\t%s",head.id, Thread.currentThread().getName());
					head.stage = 23; //Stage 23 Sginal WaitFindSignal Free
					head.WaitFindSignal.signal();
					Logger.u(TAG, "Try\tUnlock\tWaitFindLock\tFree\t%s\t%s",head.id, Thread.currentThread().getName());
				} finally{
					head.stage = 24; //Stage 24 UnlockWaitFindLock;
					head.WaitFindLock.unlock();
				}
			}
		} finally{
			Logger.u(TAG, "Try\tUnlock\tAllock\tFree\t \t%s", Thread.currentThread().getName());
			Logger.o(Allocator.this);
			Investigator = (Investigator + 1) %10;
			Alloclock.unlock();
		}
	}
	public int getWorkerQueueSize(){
		return this.workerQueue.size();
	}
	public int getAllocatorSize(){
		return this.availableQueue.size();
	}
	private int Investigator = 0;
	private class ResidentExecutor extends TimerTask{
		private int PreInvestigator = 0;
		@Override
		public void run() {
			if(!workerQueue.isEmpty() && (Investigator == PreInvestigator)){
				ThreadMXBean bean = ManagementFactory.getThreadMXBean();
				Logger.d(TAG, "Dout Deadlock... Let's investigate");
				long[] threadIds = bean.findDeadlockedThreads();
				
				if (threadIds != null) {
					Logger.e(TAG, "Deadlock detected!");
				    ThreadInfo[] infos = bean.getThreadInfo(threadIds);

				    for (ThreadInfo info : infos) {
				        StackTraceElement[] stack = info.getStackTrace();
				        for(StackTraceElement ele:stack)
				        	Logger.e(TAG, "Deadlock:\t"+ele.toString());
				    }
				}
			}
			PreInvestigator = Investigator;
			/*
			if(availableQueue.size() == ThreadPool.length && workerQueue.size() > 1){
				Logger.d(TAG, "Process Resident WorkerQueue");
				Alloclock.lock();
				try{
					PollWorkerQueue();
				} finally{
					Alloclock.unlock();
					Logger.d(TAG, "Allocator Wait:%s Avail:%s", workerQueue.size(), availableQueue.size());
				}
			}*/
		}
	}
	protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}