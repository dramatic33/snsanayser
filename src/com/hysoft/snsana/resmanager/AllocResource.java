package com.hysoft.snsana.resmanager;

public interface AllocResource {
	public Object getResource();
	public void setResource(Object resource);
}
