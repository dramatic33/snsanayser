package com.hysoft.snsana.resmanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import com.hysoft.snsana.db.worker.SQLWorker;
import com.hysoft.snsana.db.worker.SQLWorkerResult;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;

public abstract class Executor implements Allocatable{
	private int id;
	
	protected AllocResource mCon;
	private volatile boolean Stopped = true;
	private ExecutorRun runner;
	ReentrantLock ReturnLock = new ReentrantLock();
	Condition ReturnSignal = ReturnLock.newCondition();
	Condition ReturnSignalOwn = ReturnLock.newCondition();
	
	protected Executor(){
		runner = new ExecutorRun();
	}
	
	public class ExecutorRun implements Runnable{	
		private WorkerResult res;
		private Worker worker;
		private Exception exception;
		@Override
		public void run() {
			try{
				worker.setResouce(mCon);
				this.res = worker.work();
			} catch (Exception e) {
				this.exception = e;
			}
			ReturnLock.lock();
			try{
				if(!ReturnLock.hasWaiters(ReturnSignal)) ReturnSignalOwn.await();
				ReturnSignal.signal();
			} catch (InterruptedException e){
				this.exception = new SQLWorkerException(e);
			}   finally {
				ReturnLock.unlock();
			}
			
		}
		public WorkerResult submit(Worker Worker) throws Exception{
			this.worker = Worker;
			new Thread(this).start();
			ReturnLock.lock();
			try {
				ReturnSignalOwn.signal();
				ReturnSignal.await();
				if(this.exception!=null){
					throw this.exception;
				}
				return res;
			} catch (InterruptedException e) {
				throw new SQLWorkerException(e);
			} finally{
				mCon = worker.getResouce();
				this.exception = null;
				this.res = null;
				this.worker = null;
				Stop();
				ReturnLock.unlock();
			}
		}
	}
	public boolean isRunning(){
		return !this.Stopped;
	}
	public void Stop(){
		this.Stopped = true;
	}
	public void Start(){
		this.Stopped = false;
	}
	public ExecutorRun getRunner(){
		return this.runner;
	}
	public void setID(int id){
		this.id = id;
	}
	public int getID(){
		return id;
	}
	public void setResouce(AllocResource Resouce){
		this.mCon = Resouce;
	}
	public AllocResource getResouce(){
		return this.mCon;
	}
}