package com.hysoft.snsana.resmanager;

public abstract class Worker {
	protected AllocResource mCon;
	abstract public WorkerResult work() throws Exception;
	public void setResouce(AllocResource res){
		this.mCon = res;
	}
	public AllocResource getResouce(){
		return this.mCon;
	}
}
