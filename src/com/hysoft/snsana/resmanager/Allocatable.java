package com.hysoft.snsana.resmanager;

public interface Allocatable{
	public Allocatable duplicate() throws Exception;
	public void setID(int id);
	public void Start();
	public void Stop();
	public int getID();
	public void setResouce(AllocResource Resouce);
	public AllocResource getResouce();
}
