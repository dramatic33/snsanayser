package com.hysoft.snsana.etc;

import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.hysoft.snsana.analysis.Analyser;
import com.hysoft.snsana.db.DB;
import com.hysoft.snsana.db.DBFactory;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.server.MainServer;
import com.hysoft.snsana.server.MainServerFactory;

public class Setter {
	private JSONObject json;
	public Setter(String FileName) throws IOException{
		this.json = (JSONObject) JSONValue.parse(new FileReader(FileName));
		/*DEBUG Setting*/
		setDebug();
		setAnalyser();
	}
	public void setDebug(){
		JSONObject debug = (JSONObject) json.get("DebugInfos");
		JSONObject elastic = (JSONObject) debug.get("Elasticresearch");
		if(elastic!=null){
			Logger.setElasticresearch(elastic.get("IP").toString(), new Integer(((Long)elastic.get("port")).intValue()));
		}
		JSONArray except = (JSONArray) debug.get("Except");
		for(Object item:except){
			Logger.DEBUG_EXCEPT_LIST.add(item.toString());
		}
		Logger.setLevel((String) debug.get("Level"));
	}
	public DB getDB() throws Exception{
		JSONObject db = (JSONObject) json.get("DBInfos");
		String IP = (String) db.get("IP");
		int port = new Integer(((Long)db.get("port")).intValue());
		String id = (String) db.get("id");
		String pass = (String) db.get("pass");
		String DBScheme = (String) db.get("DB");
		DB.THREAD_NUM = new Integer(((Long)db.get("Thread")).intValue());
		return new DBFactory().setIP(IP).setPort(port).setID(id).setPass(pass).setDB(DBScheme).build();
	}
	public MainServer getServer() throws Exception{
		JSONObject server = (JSONObject) json.get("MainServerInfos");
		int port = new Integer(((Long)server.get("port")).intValue());
		int max_thread = new Integer(((Long)server.get("max_thread")).intValue());
		boolean whitelist_enabling = (boolean) server.get("Enable_white_list");
		List<String> whitelist = parseJSONArray((JSONArray) server.get("white_list"));
		boolean blacklist_enabling =(boolean) server.get("Enable_black_list");
		List<String> blacklist = parseJSONArray((JSONArray) server.get("black_list"));
		DB db = getDB();
		MainServerFactory factory = new MainServerFactory().setDB(db).setMaxThread(max_thread).setPort(port);
		factory.EnableWhiteList(whitelist_enabling);
		for(String white:whitelist){
			factory.addWhiteIP(white);
		}
		factory.EnablBlackList(blacklist_enabling);
		for(String black:blacklist){
			factory.addBlackIP(black);
		}
		return factory.build();
	}
	public void setAnalyser(){
		JSONObject analyser = (JSONObject) json.get("AnayserInfos");
		List<String> allowTags = parseJSONArray((JSONArray) analyser.get("AllowWordTags"));
		Analyser.setAllowTags(allowTags.toArray(new String[0]));
		Analyser.THREAD_NUM = new Integer(((Long)analyser.get("Thread")).intValue());
	}
	private <T> List<T> parseJSONArray(JSONArray array){
		List<T> list = new ArrayList<T>();
		for(Object object:array){
			list.add((T)object);
		}
		return list;
	}
}
