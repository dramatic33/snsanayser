package com.hysoft.snsana.sns;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.StringSET;
import com.hysoft.snsana.sns.client.GetBuilder;
import com.hysoft.snsana.sns.client.PostBuilder;
import com.hysoft.snsana.sns.client.Responser;
import com.hysoft.snsana.sns.search.SearchQuery;
import com.hysoft.snsana.sns.search.SearchResult;
import com.hysoft.snsana.sns.search.SearchResultSET;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class Twitter extends SNS{
	private final String TAG = StringSET.SNS.TWITTER.TAG;
	private String accessToken;
	public Twitter(String loginKey) throws IOException{
		setAccessToken(loginKey);
	}
	private void setAccessToken(String LoginKey) throws IOException{
		String[] LoginInfo = LoginKey.split(";");
		String BearerToken = LoginInfo[0]+":"+LoginInfo[1];
		String base64Encode = Base64.encode(BearerToken.getBytes()).replace("\n", "");
		
		PostBuilder pb = new PostBuilder();
		pb.addHeader("Authorization", "Basic "+base64Encode);
		pb.addHeader("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
		pb.setContent("grant_type=client_credentials");
		pb.Post("https://api.twitter.com/oauth2/token", new Responser(){
			@Override
			public void processRespone(InputStream is) throws IOException {
				JSONObject json = (JSONObject) JSONValue.parse(new InputStreamReader(is));
				accessToken = (String) json.get("access_token");
				Logger.i(TAG, StringSET.SNS.TWITTER.AUTH, accessToken);
			}
		});
	}
	@Override
	public SearchResultSET search(SearchQuery query) throws IOException, ParseException {
		GetBuilder gb = new GetBuilder();
		gb.addHeader("Authorization", "Bearer "+accessToken);
		final StringBuilder res = new StringBuilder();
		String URLEncodeKey = URLEncoder.encode(query.getKey()+" -RT", "UTF-8");
		String URL = "https://api.twitter.com/1.1/search/tweets.json?q="+URLEncodeKey+"&count="+query.getCount();
		try{
			gb.Get(URL, new Responser(){
				@Override
				public void processRespone(InputStream is) throws IOException {
					BufferedReader br = new BufferedReader(new InputStreamReader(is));
					String line = null;
					while((line=br.readLine())!=null){
						res.append(line);
					}
				}
			});
		} catch (IOException e){
			
		}
		try{
			JSONObject json = (JSONObject) JSONValue.parse(res.toString());
			JSONArray list = (JSONArray) json.get("statuses");
			SearchResultSET set = new SearchResultSET();
			SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZZ yyyy", Locale.ENGLISH);
			for(Object twitt:list){
				String dateText = (String) ((JSONObject) twitt).get("created_at");
				String text = (String) ((JSONObject) twitt).get("text");
				set.add(new SearchResult(removeMetion(removeSurrogateArea(text)), format.parse(dateText)));
			}
			return set;
		} catch (NullPointerException e){
			return null;
		}
		
		
	}
	final Pattern MetionRemoveP = Pattern.compile("(@[^\\s]+)");
	final Pattern EmojiRemoveP = Pattern.compile("[^\\x00-\\x7F]");

	private String removeMetion(String Twitt){ 
		Matcher m = MetionRemoveP.matcher(Twitt);
		if(m.find()){
			return Twitt.replace(m.group(1)+" ", "");
		} else {
			return Twitt;
		}
	}
	 public static String removeSurrogateArea(String val)
	 {
	  if ( val == null ) return null;
	  
	  StringBuffer buf = new StringBuffer();
	  int len = val.length();
	  for ( int i = 0 ; i < len ; i++ )
	  {
	   char c = val.charAt(i);
	   if ( 0xD800 <= c && c <= 0xDBFF || 
	     0xDC00 <= c && c <= 0xDFFF )
	   {
	   }
	   else
	   {
	    buf.append(c);
	   }
	  }
	  return buf.toString();
	 }

}
