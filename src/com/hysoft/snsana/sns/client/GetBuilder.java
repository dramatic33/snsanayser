package com.hysoft.snsana.sns.client;

import java.io.IOException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import com.hysoft.snsana.debug.Exceptions.AuthExpiredException;
import com.hysoft.snsana.etc.Pair;

public class GetBuilder extends RequestBuilder{
	public void Get(String URL, Responser res) throws IOException{
		 URL url = new URL(URL);
	     HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
	     con.setRequestMethod("GET");
	     con.setDoInput(true);
	     for(Pair item:Headers){
		     con.setRequestProperty(item.key, item.value);
	     }
	     if(Content!=null){
	    	 con.setDoOutput(true);
		     con.getOutputStream().write(Content.getBytes());
		     con.getOutputStream().flush();
		     con.getOutputStream().close();
	     }
	     try{
	    	 res.processRespone(con.getInputStream()); 
	     } catch (IOException e){
	    	 if(con.getResponseCode()==503){
	    		 throw new AuthExpiredException();
	    	 } else {
	    		 throw e;
	    	 }
	     }
	     
	     con.disconnect();
	}
}
