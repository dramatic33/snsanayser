package com.hysoft.snsana.sns.client;

import java.util.ArrayList;
import java.util.List;

import com.hysoft.snsana.etc.Pair;

public class RequestBuilder {
	protected List<Pair> Headers = new ArrayList<Pair>();
	protected String Content;
	public void addHeader(String id, String value){
		Pair pair = new Pair(id, value);
		Headers.add(pair);
	}
	public void setContent(String content){
		Content = content;
	}
}
