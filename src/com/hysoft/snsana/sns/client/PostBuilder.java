package com.hysoft.snsana.sns.client;

import java.io.IOException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import com.hysoft.snsana.etc.Pair;

public class PostBuilder extends RequestBuilder{
	public PostBuilder(){
		
	}
	public void Post(String URL, Responser res) throws IOException{
		 URL url = new URL(URL);
	     HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
	     con.setRequestMethod("POST");
	     con.setDoInput(true);
	     for(Pair item:Headers){
		     con.setRequestProperty(item.key, item.value);
	     }
	     if(Content!=null){
	    	 con.setDoOutput(true);
		     con.getOutputStream().write(Content.getBytes());
		     con.getOutputStream().flush();
		     con.getOutputStream().close();
	     }
	     res.processRespone(con.getInputStream());
	     con.disconnect();
	}
}
