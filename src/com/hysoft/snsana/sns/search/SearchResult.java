package com.hysoft.snsana.sns.search;

import java.util.Date;

public class SearchResult {
	private String Comment;
	private Date Date;
	public SearchResult(String Comment, Date Date){
		this.Comment = Comment;
		this.Date = Date;
	}
	public String getComment(){
		return Comment;
	}
	public Date getDate(){
		return Date;
	}
	public void CommentProcess(){
		this.Comment = this.Comment.replaceAll("\n", " ");
	}
}
