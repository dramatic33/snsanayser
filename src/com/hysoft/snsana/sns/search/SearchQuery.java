package com.hysoft.snsana.sns.search;

import java.util.Date;

public class SearchQuery {
	private String key = null;
	private int count = 15;
	private Date fromDate;
	private Date toDate;
	public SearchQuery(String key){
		this.key = key;
	}
	public void setFromDate(Date date){
		this.fromDate = date;
	}
	public void setToDate(Date date){
		this.toDate = date;
	}
	public void setCount(int Count){
		this.count = Count;
	}
	public String getKey(){
		return key;
	}
	public int getCount(){
		return count;
	}
	public Date getFromDate(){
		return this.fromDate;
	}
	public Date getToDate(){
		return this.toDate;
	}
}
