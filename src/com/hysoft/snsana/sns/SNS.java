package com.hysoft.snsana.sns;

import java.io.IOException;
import java.text.ParseException;

import com.hysoft.snsana.sns.search.SearchQuery;
import com.hysoft.snsana.sns.search.SearchResultSET;

public abstract class SNS {
	public enum Base {TWITTER};
	protected SNS(){

	}
	public abstract SearchResultSET search(SearchQuery query) throws IOException, ParseException;
}
