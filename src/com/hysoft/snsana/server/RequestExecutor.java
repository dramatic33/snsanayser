package com.hysoft.snsana.server;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.hysoft.snsana.analysis.PreProcessing;
import com.hysoft.snsana.analysis.SNSComment;
import com.hysoft.snsana.analysis.score.AcceptLowScoringPolict;
import com.hysoft.snsana.analysis.score.DefaultScoringPolicy;
import com.hysoft.snsana.analysis.score.Scorer;
import com.hysoft.snsana.analysis.score.ScoringPolicy;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.Exceptions.AbscentFieldException;
import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.debug.Exceptions.GetterServerException;
import com.hysoft.snsana.debug.Exceptions.SNSServerException;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.server.getter.Getter;
import com.hysoft.snsana.server.getter.GetterServer;

public class RequestExecutor {
	public static Response execute(Request request) throws Exception{
		int code = Integer.valueOf(request.getBody().ReadItem("code"));
		Logger.v("REQ", CommandName(code));
		switch(code){
			case 400: return ReqBestSentList(request);
			case 401: return ReqWorstSentList(request);
			case 500: return ReqProcessResComment(request);
			case 600: return ReqStartGetterServer(request);
			case 601: return ReqStopGetterServer(request);
			case 602: return ReqNewGetter(request);
			case 603: return ReqRemoveGetter(request);
			case 901: return ReqEnableDebugTag(request);
			case 902: return ReqDisableDebugTag(request);
			default: return new Response().setFailCause(new Exception("Unexpected request code"));
		}
	}
	private static String CommandName(int code){
		switch(code){
			case 400: return "Best fittest Sentence";
			case 500: return "Process resident unpreprocessed sentence";
			case 600: return "Start Getter Server";
			case 601: return "End Getter Server";
			case 602: return "Add new Getter";
			case 603: return "Remove Getter";
			case 901: return "Enable Debug Tag";
			case 902: return "Disable Debug Tag";
			default: return "Unexpected Code";
		}
	}
	@SuppressWarnings("unchecked")
	private static String JSONEncode(Object Body) throws IOException{
		JSONObject object = new JSONObject();
		object.put("result", "ok");
		if(Body!=null) object.put("returned", Body);
		StringWriter sw = new StringWriter();
		object.writeJSONString(sw);
		
		return sw.toString();
	}
	private static Response ReqBestSentList(Request request) throws SNSServerException, SQLWorkerException, IOException{
		int cate_id = Integer.valueOf(request.getBody().ReadItem("cate_id"));
		List<SNSComment> ret = null;
		Scorer scorer = new Scorer(cate_id);
		ScoringPolicy policy = new DefaultScoringPolicy();
		try{
			int max_res = Integer.valueOf(request.getBody().ReadItem("max_res"));
			ret = scorer.getBestSentence(MainServer.mDB, policy, max_res);
		} catch (AbscentFieldException e){
			if(e.getField().equals("max_res")){
				ret = scorer.getBestSentence(MainServer.mDB, policy);
			} else {
				throw e;
			}
		}
		return EncodeSentList(ret);
	}
	private static HashMap<Integer, List<SNSComment>> WorstHash = new HashMap<Integer, List<SNSComment>>();
	private static Response ReqWorstSentList(Request request) throws NumberFormatException, SNSServerException, SQLWorkerException, IOException{
		int cate_id = Integer.valueOf(request.getBody().ReadItem("cate_id"));
		List<SNSComment> ret = null;
		if(!WorstHash.containsKey(cate_id)){
			Scorer scorer = new Scorer(cate_id);
			ScoringPolicy policy = new AcceptLowScoringPolict();
			ret = scorer.getWorstSentence(MainServer.mDB, policy, -1);
			WorstHash.put(cate_id, ret);
			
		} else {
			ret = WorstHash.get(cate_id);
		}
		List<SNSComment> sub = new ArrayList<SNSComment>();
		int score_limit = -1;
		try{
			score_limit = Integer.valueOf(request.getBody().ReadItem("score_limit"));
		} catch (AbscentFieldException e){
			
		}
		Iterator<SNSComment> iter = ret.iterator();
		while(iter.hasNext()){
			SNSComment com = iter.next();
			if(score_limit!=-1 && com.getScore() > score_limit) sub.add(com);
		}
		int from = Integer.valueOf(request.getBody().ReadItem("from"));
		int to = Integer.valueOf(request.getBody().ReadItem("to"));
		
		sub = sub.subList(from, to);
		
		return EncodeSentList(sub);
		
	}
	private static Response EncodeSentList(List<SNSComment> ret) throws IOException{
		JSONArray array = new JSONArray();
		for(SNSComment item:ret){
			JSONObject object = new JSONObject();
			object.put("id", item.getID());
			object.put("comment", item.getComment());
			object.put("score", item.getScore());
			array.add(object);
		}
		String Body = JSONEncode(array);
		
		return new Response().setBody(Body);
	}
	private static Response ReqProcessResComment(Request request) throws SNSServerException, SQLWorkerException, AnalysisException, IOException{
		int interval = Integer.valueOf(request.getBody().ReadItem("interval"));
		int key_id = Integer.valueOf(request.getBody().ReadItem("key_id"));
		new Thread(new PreProcessing(key_id, interval)).start();
		return new Response().setBody(JSONEncode(null));
	}
	private static Response ReqStopGetterServer(Request request) throws GetterServerException, IOException {
		MainServer.getter.stop();
		MainServer.getter = null;
		return new Response().setBody(JSONEncode(null));
	}
	private static Response ReqStartGetterServer(Request request) throws GetterServerException, IOException, SQLWorkerException {
		MainServer.getter = new GetterServer(MainServer.mDB);
		MainServer.getter.start();
		return new Response().setBody(JSONEncode(null));
	}
	private static Response ReqNewGetter(Request request) throws NumberFormatException, SNSServerException, SQLWorkerException, IOException{
		int id = Integer.valueOf(request.getBody().ReadItem("key_id"));
		Getter getter = MainServer.mDB.DBGetter.getGetter(id);
		MainServer.getter.addGetter(getter);
		return new Response().setBody(JSONEncode(null));
	}
	private static Response ReqRemoveGetter(Request request) throws IOException, NumberFormatException, SNSServerException{
		int id = Integer.valueOf(request.getBody().ReadItem("key_id"));
		MainServer.getter.removeGetter(id);
		return new Response().setBody(JSONEncode(null));
	}
	private static Response ReqEnableDebugTag(Request request) throws SNSServerException, IOException{
		String tag = request.getBody().ReadItem("tag");
		if(Logger.DEBUG_EXCEPT_LIST.contains(tag)) Logger.DEBUG_EXCEPT_LIST.remove(tag);
		return new Response().setBody(JSONEncode(null));
	}
	private static Response ReqDisableDebugTag(Request request) throws IOException, SNSServerException{
		String tag = request.getBody().ReadItem("tag");
		if(!Logger.DEBUG_EXCEPT_LIST.contains(tag)) Logger.DEBUG_EXCEPT_LIST.add(tag);
		return new Response().setBody(JSONEncode(null));
	}
}
