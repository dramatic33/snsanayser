package com.hysoft.snsana.server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import com.hysoft.snsana.db.*;

public class MainServerFactory {
	//Basic Setting
	private static int PORT = 8821;
	private static int ThreadNum = 4;
	public MainServerFactory setPort(int port){
		PORT = port;
		return this;
	}
	public MainServerFactory setMaxThread(int maxThread){
		ThreadNum = maxThread;
		return this;
	}
	//DB Setting
	private DB mDB = null;
	public MainServerFactory setDB(DB DB){
		mDB = DB;
		return this;
	}
	//Restrict Setting
	private ArrayList<InetAddress> WhiteListIP;
	private ArrayList<InetAddress> BlackListIP;
	public MainServerFactory EnableWhiteList(boolean enable){
		if(enable) WhiteListIP = new ArrayList<InetAddress>();
		return this;
	}
	public MainServerFactory addWhiteIP(String IP) throws UnknownHostException{
		if(WhiteListIP!=null) WhiteListIP.add(InetAddress.getByName(IP));
		return this;
	}
	public MainServerFactory EnablBlackList(boolean enable){
		if(enable) BlackListIP = new ArrayList<InetAddress>();
		return this;
	}
	public MainServerFactory addBlackIP(String IP) throws UnknownHostException{
		if(BlackListIP!=null) BlackListIP.add(InetAddress.getByName(IP));
		return this;
	}
	public MainServer build() throws Exception{
		//if(mDB==null) throw new SNSServerException(StringSET.DB.NULL);
		MainServer server = new MainServer(PORT, mDB, ThreadNum);
		server.setWhiteList(WhiteListIP);
		server.setBlackList(BlackListIP);
		return server;
	}
}
