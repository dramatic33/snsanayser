package com.hysoft.snsana.server;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.json.simple.JSONObject;

public class Response {
	private Exception FailCause = null;
	private String Body = "";
	
	public void write(OutputStream os) throws IOException{
		PrintWriter pw = new PrintWriter(os);
		this.writeOK(pw);
		pw.println("Content-Type: application/json; charset=utf-8");
		writeBody(pw);
		pw.flush();
		pw.close();
	}
	@SuppressWarnings("unchecked")
	private void writeBody(PrintWriter pw) throws IOException{
		if(FailCause==null){
			pw.println("Content-Length: "+Body.getBytes().length);
			pw.println("");
			pw.println(Body);
		} else{
			JSONObject obj = new JSONObject();
			obj.put("result", "fail");
			obj.put("reason", FailCause.getMessage());
			StringWriter sw = new StringWriter();
			obj.writeJSONString(sw);
			String res = sw.toString();
			
			pw.println("Content-Length: "+res.getBytes().length);
			pw.println("");
			pw.println(res);
		}
	}
	public Response setBody(String Body){
		this.Body = Body;
		return this;
	}
	public Response setFailCause(Exception cause){
		this.FailCause = cause;
		return this;
	}
	private void writeOK(PrintWriter pw){
		if(FailCause==null){
			pw.println("HTTP/1.1 200 OK");
		}  else {
			pw.println("HTTP/1.1 200 OK");
		}
	}
}
