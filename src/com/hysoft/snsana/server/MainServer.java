package com.hysoft.snsana.server;

import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.hysoft.snsana.analysis.Analyser;
import com.hysoft.snsana.db.DB;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.StringSET;
import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.debug.Exceptions.GetterServerException;
import com.hysoft.snsana.debug.Exceptions.SNSServerException;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.server.getter.GetterServer;

public class MainServer implements Runnable{
	private int MaxThread;
	private int port;
	
	private ServerSocket listener = null;
	public static DB mDB;
	public static Analyser analyser;
	private static ExecutorService ThreadPool;
	public static GetterServer getter;
	private final String TAG = StringSET.SERVER.TAG;
	private List<InetAddress> WhiteList;
	private List<InetAddress> BlackList;
	
	public MainServer(int port, DB DB, int MaxThread) throws Exception{
		Logger.i(TAG, StringSET.SERVER.INIT);
		this.MaxThread = MaxThread;
		this.port = port;
		mDB = DB;
		analyser = new Analyser();
		getter = new GetterServer(DB);
	}
	public void setWhiteList(List<InetAddress> WhiteList){
		this.WhiteList = WhiteList;
	}
	public void setBlackList(List<InetAddress> BlackList){
		this.BlackList = BlackList;
	}
	public void start() throws IOException{
		Logger.i(TAG, StringSET.SERVER.START);
		Logger.i(TAG, "Open port %s", port);
		listener = new ServerSocket(port);
		ThreadPool = Executors.newFixedThreadPool(MaxThread);
		new Thread(this).start();
	}
	public void stop(){
		Logger.i(TAG, StringSET.SERVER.STOP);
		ThreadPool.shutdown();
		ThreadPool = null;
		System.gc();
	}
	public void StartGetting() throws GetterServerException, SQLWorkerException{
		getter.start();
	}
	public void WhiteListCheck(InetAddress address) throws SNSServerException{
		if(WhiteList!=null && !WhiteList.contains(address)) throw new SNSServerException(address.toString()+" is not in whitelist");
	}
	public void BlackListCheck(InetAddress address) throws SNSServerException{
		if(BlackList!=null && BlackList.contains(address)) throw new SNSServerException(address.toString()+" is in blacklist");
	}
	@Override
	public void run() {
		while(true){
			try{
				Socket socket = null;
				socket = listener.accept();
				ThreadPool.execute(new Worker(socket));
			} catch (IOException e){
				Logger.e(TAG, e);
			}
		}
	}
	private class Worker implements Runnable{
		private Socket socket;
		private Worker(Socket socket){
			this.socket = socket;
		}
		@Override
		public void run() {
			try {
				InetAddress address = socket.getInetAddress();
				Logger.v(TAG, StringSET.SERVER.ACCEPT, address);
				WhiteListCheck(socket.getInetAddress());
				BlackListCheck(socket.getInetAddress());
				Request req = new Request(socket.getInputStream());
				Logger.v(StringSET.SERVER.TAG, "Request:\t"+req.getBody().print());
				try{
					Response res = RequestExecutor.execute(req);
					res.write(socket.getOutputStream());
				} catch(Exception e){
					Response res = new Response().setFailCause(e);
					res.write(socket.getOutputStream());
					throw e;
				}
			} catch(SQLWorkerException e){
				Logger.e(StringSET.DB.TAG, e);
			} catch(AnalysisException e){
				Logger.e(StringSET.ANA.TAG, e);
			} catch (Exception e) {
				Logger.e(TAG, e);
			} finally{
				if(socket!=null){
					try{socket.close();} catch (IOException e){}
				}
				Logger.v(TAG, StringSET.SERVER.DISCONNECT, socket.getInetAddress());
			}
		}
		
	}
}
