package com.hysoft.snsana.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.StringSET;
import com.hysoft.snsana.debug.Exceptions.AbscentFieldException;
import com.hysoft.snsana.debug.Exceptions.SNSServerException;
import com.hysoft.snsana.etc.Pair;

public class Request {
	private Header Header;
	private Body Body;
	public Request(InputStream is) throws IOException, SNSServerException{
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		Header = new Header(br);
		try{
			int length = Integer.valueOf(Header.ReadItem("Content-Length"));
			Body = new Body(br, length);
		} catch (IllegalArgumentException e){
			Body = new Body(br);
		}
	}
	public Header getHeader(){
		return this.Header;
	}
	public Body getBody(){
		return this.Body;
	}
	public class Body extends PairList{
		private final static String regex = "([^=]+)=([^&]+)&?";
		private Body(BufferedReader br, int ContentLength) throws IOException{
			super(regex);
			Parse(Read(br, ContentLength));			
		}
		private Body(BufferedReader br) throws IOException{
			super(regex);
			Parse(Read(br));			
		}
		private String Read(BufferedReader br, int ContentLength) throws IOException{
			char[] buf = new char[ContentLength];
			if(br.read(buf, 0, ContentLength)<=0)
				throw new IOException();
			return new String(buf);
		}
		private String Read(BufferedReader br) throws IOException{
			StringBuilder sb = new StringBuilder();
			char[] buf = new char[256];
			while(br.read(buf)!=-1){
				sb.append(new String(buf));
			}
			return sb.toString();
		}
	}
	public class Header extends PairList{
		private final static String regex = "([^\\n|:]+): ([^\\n]+)\\n?";
		private String method = null;
		private Header(BufferedReader br) throws IOException{
			super(regex);
			Parse(Read(br));
		}
		private String Read(BufferedReader br) throws IOException{
			String line = null;
			StringBuilder sb = new StringBuilder();
			while(!(line = br.readLine()).equals("")){
				if(method==null) method = line;
				sb.append(line+"\n");
			}
			return sb.toString();
		}
		public String getMethod(){
			return method;
		}
	}
	public abstract class PairList{
		private Pattern ParsePattern;
		private PairList(String Regex){
			ParsePattern = Pattern.compile(Regex);
		}
		protected LinkedList<Pair> list = new LinkedList<Pair>();
		public String ReadItem(String key) throws SNSServerException{
			Iterator<Pair> iter = list.iterator();
			while(iter.hasNext()){
				Pair item = iter.next();
				if(item.key.equals(key)) return item.value;
			}
			throw new AbscentFieldException(key);
		}
		public String print(){
			Iterator<Pair> iter = list.iterator();
			StringBuilder sb = new StringBuilder();
			while(iter.hasNext()){
				Pair item = iter.next();
				sb.append("["+item.key+"]="+item.value+"&");
			}
			if(sb.charAt(sb.length()-1)=='&') return sb.substring(0, sb.length()-1);
			else return sb.toString();
		}
		protected void Parse(String Content){
			Matcher match = ParsePattern.matcher(Content);
			while(match.find()){
				list.add(new Pair(match.group(1), match.group(2)));
			}
		}
	}
}
