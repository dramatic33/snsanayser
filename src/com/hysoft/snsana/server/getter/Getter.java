package com.hysoft.snsana.server.getter;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.hysoft.snsana.analysis.SNSComment;
import com.hysoft.snsana.analysis.Word;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.StringSET;
import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.debug.Exceptions.AuthExpiredException;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.server.MainServer;
import com.hysoft.snsana.sns.SNS;
import com.hysoft.snsana.sns.search.SearchQuery;
import com.hysoft.snsana.sns.search.SearchResult;
import com.hysoft.snsana.sns.search.SearchResultSET;
import com.sun.webkit.Timer;

public class Getter extends TimerTask{
	private final String TAG = StringSET.GET.TAG;
	private final static int THREAD_NUM = 10;
	
	private Date FromTime = null;
	private Date ToTime = null;
	private int id;
	private int NullCate;
	private String keyword;
	private int count;
	public Getter(int id, String keyword, int NullCate) throws SQLWorkerException{
		this.id = id;
		this.keyword = keyword;
		this.NullCate = NullCate;
	}
	public void SetDate(Date From, Date To){
		this.FromTime = From;
		this.ToTime = To;
	}
	public Date getFromTime(){
		if(this.FromTime!=null){
			return getCurrentDayTime(this.FromTime);
		} else {
			return null;
		}
	}
	public Getter(Getter getter){
		this.FromTime = getter.FromTime;
		this.ToTime = getter.ToTime;
		this.id = getter.id;
		this.NullCate = getter.NullCate;
		this.keyword = getter.keyword;
		this.count = getter.count;
	}
	public void setCount(int Count){
		this.count = Count;
	}
	public Date getToTime(){
		if(this.ToTime!=null){
			return getCurrentDayTime(this.ToTime);
		} else {
			return null;
		}
	}
	public int getID(){
		return this.id;
	}
	@Override
	public void run() {
		try{
			Thread.currentThread().setName(keyword+" Getter");
			Logger.i(TAG, StringSET.GET.START, id);
			SNS sns = MainServer.mDB.DBSNS.getSNSFromKey(id);
			SearchQuery query = new SearchQuery(keyword);
			query.setCount(count);
			LinkedList<SearchResult> ResQueue = new LinkedList<SearchResult>();
			Date endTime = this.getToTime();
			ExecutorService executor = Executors.newFixedThreadPool(THREAD_NUM);
			while(GetterServer.running && (getToTime()==null || new Date().before(endTime))){
				try{
					SearchResultSET set = sns.search(query);
					List<InsertSentRunner> runnerList = new ArrayList<InsertSentRunner>();
					if(set!=null){
						Date recentDate = ResQueue.isEmpty()? null: ResQueue.getLast().getDate();
						ListIterator<SearchResult> iterator = set.listIterator(set.size());
						while(iterator.hasPrevious()){
							SearchResult res = iterator.previous();
							res.CommentProcess();
							if(recentDate==null || res.getDate().after(recentDate)){
								ResQueue.add(res);
								runnerList.add(new InsertSentRunner(res));
							}
						}
						while(ResQueue.size()>count){
							ResQueue.pollFirst();
						}
					}
					executor.invokeAll(runnerList);
					long sleep = 1000*5;
					Thread.sleep(sleep);
				} catch (ConnectException e){
					Logger.e(TAG, e);
					Logger.e(TAG, "Retry after 15min");
					long sleep = 1000*60*15;
					Thread.sleep(sleep);
				} catch (AuthExpiredException e){
					Logger.e(TAG, e);
					Logger.e(TAG, "Auth Expired reauth...");
					sns = MainServer.mDB.DBSNS.getSNSFromKey(id);
					long sleep = 1000*5;
					Thread.sleep(sleep);
				}
			}
			Logger.i(TAG, StringSET.GET.END, id);
			if(this.FromTime!=null && this.ToTime!=null){
				Logger.i(TAG, StringSET.GET.INFORM_NEXT, id, getFromTime(), getToTime());
			}
		} catch (Exception e){
			Logger.e(TAG, e);
		}
	}
	public static Date getCurrentDayTime(Date Time){
		if(Time!=null){
			Calendar cal = Calendar.getInstance();
			Calendar EndCal = Calendar.getInstance();
			EndCal.setTime(Time);
			cal.set(Calendar.HOUR_OF_DAY, EndCal.get(Calendar.HOUR_OF_DAY));
			cal.set(Calendar.MINUTE, EndCal.get(Calendar.MINUTE));
			cal.set(Calendar.SECOND, 0);
			if(cal.getTime().before(new Date())) cal.add(Calendar.DATE, 1);
			return cal.getTime();
		} else {
			return null;
		}
	}
	public static boolean between(Getter getter){
		Calendar fromCal = Calendar.getInstance();
		Calendar toCal = Calendar.getInstance();
		fromCal.setTime(getter.getFromTime());
		toCal.setTime(getter.getToTime());
		fromCal.add(Calendar.DATE, -1);
		if(toCal.get(Calendar.DATE)==fromCal.get(Calendar.DATE)){
			return true;
		} else {
			return false;
		}
	}
	public int getNullCate(){
		return this.NullCate;
	}
	private class InsertSentRunner implements Callable<Object>{
		SearchResult res;
		private InsertSentRunner(SearchResult res){
			this.res = res;
		}
		@Override
		public Object call(){
			SNSComment comment;
			try {
				comment = MainServer.mDB.DBSNS.addSearchResult(res, id, keyword);
				if(comment!=null){
					Logger.o(comment);
					MainServer.mDB.DBCategory.addSentenceWord(comment);
					if(!comment.contain(keyword)) MainServer.mDB.DBCategory.Categorize(comment.getID(), NullCate);
				}
			} catch (Exception e) {
				Logger.e(TAG, e);
			}
			return null;
		}
		
	}
}
