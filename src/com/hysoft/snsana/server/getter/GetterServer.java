package com.hysoft.snsana.server.getter;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;

import com.hysoft.snsana.db.DB;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.StringSET;
import com.hysoft.snsana.debug.Exceptions.GetterServerException;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;

public class GetterServer { 
	public static boolean running = false;
	private static String TAG = StringSET.GET.TAG+"SEV";
	private final static long ONCE_PER_DAY = 1000*60*60*24;
	private List<Getter> list=null;
	private HashMap<Integer, Timer> hash = new HashMap<Integer,Timer>();
	public GetterServer(DB db) throws SQLWorkerException{
		list = db.DBGetter.getGetterList();
	}
	public void start() throws GetterServerException, SQLWorkerException{
		if(running) throw new GetterServerException("Server is running already");
		running = true;
		for(Getter item : list){
			addGetter(item);
		}
		Logger.i(TAG, StringSET.GET.START_SERVER);
	}
	public void stop() throws GetterServerException{
		if(!running) throw new GetterServerException("Server is not running");
		running = false;
		Iterator<Timer> iter = hash.values().iterator();
		while(iter.hasNext()){
			iter.next().cancel();
		}
		Logger.i(TAG, StringSET.GET.STOP_SERVER);
	}
	public void addGetter(Getter item) throws SQLWorkerException{
		Timer mTimer = new Timer();
		hash.put(item.getID(), mTimer);
		if(item.getFromTime()!=null && item.getFromTime()!=null){
			Logger.i(TAG, StringSET.GET.NEW_FIXED, item.getID(), item.getFromTime(), item.getToTime());
			mTimer.scheduleAtFixedRate(item, item.getFromTime(), ONCE_PER_DAY);
			if(Getter.between(item)){
				mTimer.schedule(new Getter(item), 0);
			}
		}else{
			Logger.i(TAG, StringSET.GET.NEW_ALWAYS, item.getID());
			mTimer.schedule(item, 0);
		}
	}
	public void removeGetter(int key_id){
		Logger.i(TAG, StringSET.GET.REMOVE, key_id);
		hash.get(key_id).cancel();
		hash.put(key_id, null);
	}
}
