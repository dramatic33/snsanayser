import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Queue;

import kr.ac.kaist.swrc.jhannanum.comm.Eojeol;
import kr.ac.kaist.swrc.jhannanum.comm.Sentence;
import kr.ac.kaist.swrc.jhannanum.comm.SetOfSentences;
import kr.ac.kaist.swrc.jhannanum.hannanum.Workflow;
import kr.ac.kaist.swrc.jhannanum.plugin.MajorPlugin.MorphAnalyzer.ChartMorphAnalyzer.ChartMorphAnalyzer;
import kr.ac.kaist.swrc.jhannanum.plugin.MajorPlugin.PosTagger.HmmPosTagger.HMMTagger;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.MorphemeProcessor.UnknownMorphProcessor.UnknownProcessor;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PlainTextProcessor.InformalSentenceFilter.InformalSentenceFilter;
import kr.ac.kaist.swrc.jhannanum.plugin.SupplementPlugin.PlainTextProcessor.SentenceSegmentor.SentenceSegmentor;

import com.hysoft.snsana.analysis.Analyser;
import com.hysoft.snsana.analysis.SNSComment;
import com.hysoft.snsana.analysis.Word;
import com.hysoft.snsana.analysis.score.DefaultScoringPolicy;
import com.hysoft.snsana.analysis.score.Scorer;
import com.hysoft.snsana.db.DB;
import com.hysoft.snsana.debug.Logger;
import com.hysoft.snsana.debug.Exceptions.AnalysisException;
import com.hysoft.snsana.debug.Exceptions.SQLWorkerException;
import com.hysoft.snsana.etc.Setter;
import com.hysoft.snsana.server.MainServer;
import com.hysoft.snsana.server.MainServerFactory;
import com.hysoft.snsana.server.getter.Getter;
import com.hysoft.snsana.server.getter.GetterServer;
import com.hysoft.snsana.sns.SNS;
import com.hysoft.snsana.sns.Twitter;
import com.hysoft.snsana.sns.search.SearchQuery;
import com.hysoft.snsana.sns.search.SearchResult;
import com.hysoft.snsana.sns.search.SearchResultSET;

public class Tester {

	public static void main(String[] args) {

		try {
			Setter setter = new Setter("setting.json");
			MainServer server = setter.getServer();
			server.start();
			//server.StartGetting();*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}



